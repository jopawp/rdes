#!/usr/bin/ruby
#
# Author: Matthew A. Weidner
# Date: 01/22/2016
#
# Generate a random 56-bit key for use with the DES algorithm.
# Output is base64 encoded for ease of transmission.

# Linux provides a cyptographically secure PRNG /dev/urandom
# http://security.stackexchange.com/questions/3936/is-a-rand-from-dev-urandom-secure-for-a-login-key
f = File.open("/dev/urandom", 'r')

# read 8 bytes (64 bits)l
k = f.read(8).unpack('Q')
f.close
#puts "k = #{k[0]}"

# print in binary representation.
#print "k = "
#puts k[0].to_s(2)

# convert to base64
s = [k.to_s].pack('m') # print in base 64
puts s

# convert base 64 string back to 64-bit integer
print "k = "
puts s.unpack('m').first

