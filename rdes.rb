#!/usr/bin/ruby
#
# Author: Matthew A. Weidner
# Date: 01/22/2016
#
# Generate a random 56-bit key for use with the DES algorithm.
# Output is base64 encoded for ease of transmission.

# Linux provides a cyptographically secure PRNG /dev/urandom
# http://security.stackexchange.com/questions/3936/is-a-rand-from-dev-urandom-secure-for-a-login-key

VER = "0.1"

ik = 0
pt = []
ct = []
# 48-bit subkeys K1-K16
subkeys = []
# Initial Permutation matrix
IP = [	57, 49, 41, 33, 25, 17, 9, 1,
	   59, 51, 43, 35, 27, 19, 11, 3,
	   61, 53, 45, 37, 29, 21, 13, 5,
	   63, 55, 47, 39, 31, 23, 15, 7,
	   56, 48, 40, 32, 24, 16, 8, 0,
	   58, 50, 42, 34, 26, 18, 10, 2,
	   60, 52, 44, 36, 28, 20, 12, 4,
	   62, 54, 46, 38, 30, 22, 14, 6
]

# LK "left" 28-bit key half
LK = [	56, 48, 40, 32, 24, 16, 8,
	   0, 57, 49, 41, 33, 25, 17,
	   9, 1, 58, 50, 42, 34, 26,
	   18, 10, 2, 59, 51, 43, 35
]

# RK "left" 28-bit key half
RK = [	62, 54, 46, 38, 30, 22, 14,
	   6, 61, 53, 45, 37, 29, 21,
	   13, 5, 60, 52, 44, 36, 28, 
	   20, 12, 4, 27, 19, 11, 3
]

# L0 "left" 32-bit half-block matrix
L0 = [	57, 49, 41, 33, 25, 17, 9, 1,
	   59, 51, 43, 35, 27, 19, 11, 3,
	   61, 53, 45, 37, 29, 21, 13, 5,
	   63, 55, 47, 39, 31, 23, 15, 7
]
# R0 "right" 32-bit half-block matrix
R0 = [	56, 48, 40, 32, 24, 16, 8, 0,
	   58, 50, 42, 34, 26, 18, 10, 2,
	   60, 52, 44, 36, 28, 20, 12, 4,
	   62, 54, 46, 38, 30, 22, 14, 6
]

# Expansion matrix
E = [	31, 0, 1, 2, 3, 4,
	  3, 4, 5, 6, 7, 8,
	  7, 8, 9, 10, 11, 12,
	  11, 12, 13, 14, 15, 16,
	  15, 16, 17, 18, 19, 20,
	  19, 20, 21, 22, 23, 24,
	  23, 24, 25, 26, 27, 28,
	  27, 28, 29, 30, 31, 0
]

# S-box matrices map a 6-bit input value to a 4-bit value.
# The first and last bit of the input value select the row.
# The middle 3 bits of the input value select the column.
#
# S1 substitution "S-Box" 1 of 8
S1 = [	14, 4, 13, 2, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
	   0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
	   4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
	   15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13
]

# S2 substitution "S-Box" 2 of 8
S2 = [	15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 05, 10,
	   3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
	   0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
	   13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9
]

# S3 substitution "S-Box" 3 of 8
S3 = [	10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
	   13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
	   13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
	   1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12
]

# S4 substitution "S-Box" 4 of 8
S4 = [	7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
	   13, 8, 11, 5, 06, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
	   10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
	   3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14
]

# S5 substitution "S-Box" 5 of 8
S5 = [	2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
	   14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
	   4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
	   11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3
]

# S6 substitution "S-Box" 6 of 8
S6 = [	12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
	   10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
	   9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 01, 13, 11, 6,
	   4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13
]

# S7 substitution "S-Box" 7 of 8
S7 = [	4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
	   13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
	   1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
	   6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12
]

# S8 substitution "S-Box" 8 of 8
S8 = [	13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
	   1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
	   7, 11, 04, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
	   2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
]

# Permutation matrix
P = [	15, 6, 19, 20, 28, 11, 27, 16,
	  0, 14, 22, 25, 4, 17, 30, 9,
	  1, 7, 23, 13, 31, 26, 2, 8,
	  18, 12, 29, 5, 21, 10, 3, 24,
]

# Key permutation matrix #1
PC1 = [	56, 48, 40, 32, 24, 16, 8, 0, 57, 49, 41, 33, 25, 17,
		9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35,
		62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21,
		13, 5, 60, 52, 44, 36, 28, 20, 12, 4, 27, 19, 11, 3
]

# Key permutation matrix #2
PC2 = [	13, 16, 10, 23, 0, 4, 2, 27, 14, 5, 20, 9,
		22, 18, 11, 3, 25, 7, 15, 6, 26, 19, 12, 1,
		40, 51, 30, 36, 46, 54, 29, 39, 50, 44, 32, 47,
		43, 48, 38, 55, 33, 52, 45, 41, 49, 35, 28, 31
]

# Inverse Permutation matrix
IP1 = [	39, 7, 47, 15, 55, 23, 63, 31,
		38, 6, 46, 14, 54, 22, 62, 30,
		37, 5, 45, 13, 53, 21, 61, 29,
		36, 4, 44, 12, 52, 20, 60, 28,
		35, 3, 43, 11, 51, 19, 59, 27,
		34, 2, 42, 10, 50, 18, 58, 26,
		33, 1, 41, 9, 49, 17, 57, 25,
		32, 0, 40, 8, 48, 16, 56, 24
]

def sbox(b,p,i)
	#	puts "sbox(#{i}): #{p}"
	s = 0
	t = 0
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**i) > 0)
		s += 2**0
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**(i+5)) > 0)
		s += 2**1
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**(i+1)) > 0)
		t += 2**0
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**(i+2)) > 0)
		t += 2**1
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**(i+3)) > 0)
		t += 2**2
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"
	if ((b & 2**(i+4)) > 0)
		t += 2**3
	end
	#	puts "s: #{(s.to_s(2))}, t:#{(t.to_s(2))}"

	#	puts "p:#{(p[((16*s)+t)])}"

	return p[((16*s)+t)]
end

def sboxes(b)
	r = []
	a = 0
	i = 0
	y = 0
	r << sbox(b,S1,42)
	r << sbox(b,S2,36)
	r << sbox(b,S3,30)
	r << sbox(b,S4,24)
	r << sbox(b,S5,18)
	r << sbox(b,S6,12)
	r << sbox(b,S7,6)
	r << sbox(b,S8,0)
	while i < r.length-1
		y += r[i]
		y = y << 4
		#		puts "sboxes[#{i+1}]: #{(r[i].to_s(2))}"
		#		puts "      y: #{(y.to_s(2))}"
		i += 1
	end
	y += r[-1]
	#	puts "S(): #{(y).to_s(2)}"
	return y
end

def expand(b)
	return permute(b, E, 32)
end

def split(b, bitwidth)
	l = 0
	r = 0
	# clear unused high order bits
	if bitwidth < 64
		(bitwidth..63).each do |e|
			if ((b & 2**e) > 0)
				b -= 2**e
			end
		end
	end
	l = b
	r = b
	l = l >> bitwidth/2
	r = r - (l << bitwidth/2)
	puts "bits: #{bitwidth}"
	puts "bits/2: #{bitwidth/2}"
	puts "b: #{(b.to_s(2))}"
	puts "l: #{(l.to_s(2))}"
	puts "r: #{(r.to_s(2))}"
	return l,r
end

def permute(b, m, o_bitwidth)
	o_bitwidth -= 1
	if b.is_a? Array
		b = b[0]
	end
	r = 0
	# permutes b according to matrix m
	# iterate over permutation matrix
	n = m.length - 1
	(0..n).each do |i|
		# bit test
		#		puts "real_bit: #{(o_bitwidth - m[i])}"
		z = o_bitwidth - m[i]
		z = (2**z)
		if ((b & z) > 0)
			r += 2**(n-i)
		end
		#		puts "permute b: #{(b.to_s(2))}, o: #{o_bitwidth+1}, m: #{(m.length)}"
		#		puts "i: #{i} #{m[i]} (#{(o_bitwidth - m[i])}) z: #{z}"
		#		puts "b & z: #{b & z}"
	end
	#	puts "permute: r:#{(r.to_s(2))}"
	return r
end

def lrot(b,bitwidth)
	e = bitwidth - 1
	# clear unused high order bits
	if bitwidth < 64
		(bitwidth..63).each do |e|
			if ((b & 2**e) > 0)
				b -= 2**e
			end
		end
	end
	#	puts "b: #{(b.to_s(2))}"
	# left rotate by one bit
	if ((b & 2**e) > 0)
		b = b - 2**e
		b = b << 1
		b = b | 2**0
	else
		b = b << 1
	end

	#	puts "lrot b: #{(b.to_s(2))}"
	return b
end

def generate_subkeys(x)
	keys = []
	a = permute(x, PC1, 64)
	puts "PC1: #{(a).to_s(2)}"
	b,c = split(a, 56)
	puts "BC(i): #{(b.to_s(2))} #{(c.to_s(2))}"
	(0..15).each do |i|
		if ((i != 0) && (i != 1) && (i != 8) && (i != 15)) then
			# perform an extra rotation on this schedule.
			b = lrot(b,28)
			c = lrot(c,28)
		end
		b = lrot(b,28)
		c = lrot(c,28)
		a = (b << 28) + c
		f = permute(a, PC2, 56)
		keys << f
		puts "BC(#{i}): #{(b.to_s(2))} #{(c.to_s(2))}"
		puts "K(#{i}): #{(f.to_s(2))}"
	end
	return keys
end

def des(ik, pt, mode)
	ct = []
	# generate 16 sub keys
	subkeys = generate_subkeys(ik)
	
	if mode == "d"
		# decryption
		subkeys.reverse!
	end

	#puts "#{subkeys}"

	# encrypt/decrypt message
	(0..(pt.length-1)).each do |m|
		puts "M: #{(pt[m]).to_s(2)}"
		p = permute pt[m], IP, 64
		puts "IP: #{(p.to_s(2))}"
		l0,r0 = split(p, 64)
		puts "LR0: #{(l0.to_s(2))} #{(r0.to_s(2))}"
		# 16 round DES
		(0..15).each do |i|
			# expand 32-bit R to a 48-bit value and xor with a 48-bit sub-key
			r = expand(r0) 
			puts "i: #{i} E(R#{i}): #{(r.to_s(2))}"
			r = r ^ subkeys[i]
			puts "K(#{i} + E(R#{i}): #{(r.to_s(2))}"
			# S-boxes
			r = sboxes(r)
			puts "S(#{i}): #{(r.to_s(2))}"
			# permute
			r = permute r, P, 32
			puts "P(#{i}): #{(r.to_s(2))}"
			# prepare for next round
			r = r ^ l0
			l0 = r0
			r0 = r
			puts "R(#{(i)}): #{(r0.to_s(2))}"
			puts "L(#{(i)}): #{(l0.to_s(2))}"
		end
		c = r0 << 32
		c += l0
		#	puts "c:#{(c.to_s(2))}"
		s = c.to_s(2)
		puts "P(RL16): #{(s.gsub(/(.{8})(?=.)/, '\1 \2'))}"
		#	puts "l: #{(l0.to_s(2))}, r: #{(r0.to_s(2))}"
		c = permute c, IP1, 64
		s = c.to_s(2)
		puts "FP:#{(s.gsub(/(.{8})(?=.)/, '\1 \2'))}"
		ct << c
	end
	return ct
end


#-----------#
#			#
#	MAIN	#
#			#
#-----------#
puts "rdes.rb DES implementation #{VER}"
puts "Matthew A. Weidner <matthewweidner@snhu.edu>"
puts ""

if ARGV[0] == "t"
	puts "SANITY CHECK"
	puts "------------"
	puts "split()"
	a,b = split(268435488, 48)
	if a == 16 && b == 32
		puts "[+] PASS"
	else
		puts "[-] FAIL, a: #{a}, b: #{b}"
	end
	a,b = split(72057594054705152, 64)
	if a == 16777216 && b == 16777216
		puts "[+] PASS"
	else
		puts "[-] FAIL, a: #{a}, b: #{b}"
	end

	puts "END"
	exit
end


if ARGV[0] == nil || ARGV[1] == nil
	puts "\tPlease specify a plaintext file to encrypt and a key file with a base 64 encoded key string on the command line."
	puts "\t\tExample: rdes.rb <plaintext.txt> <key.txt>"
	exit
end

# Read key file name from 2nd command line argument
f = File.open(ARGV[1], 'r')
s = f.gets.chomp
f.close
ik = s.unpack('m')[0][1..-2].to_i

# Read plaintext message file name to encrypt from 1st command line argument
f = File.open(ARGV[0], 'r')
f.each_line do |l|
	s = l.chomp
	s.upcase!
	i = 0
	# pad message to a multiple of 8 bytes.
	while s.length % 8 > 0
		s << "X"
	end
	# split message into 8-byte (64-bit) blocks
	while i < s.length
		pt << s[i..i+7].unpack('Q')
		i += 8
	end
end
f.close
#puts "ik: #{ik}"
#puts "pt: #{pt}"
#puts pt[0]

# Official test vector, enciphers to 0EEC1487DD8C26D5
#ik = 4000000000000000
#pt = [0000000000000000]

# Homegrown test vector, "ATTACKAT" f6df6a65b78bd278
ik = 4267328009278650206
#pt = [6071216524903076929] # little-endian
pt = [4707480149787623764] # big-endian

# Homegrown test vector #1, partial hand verification
#ik = 9223372036854775809
#pt = [9367487224930631680]

# Test vector used in example case:
# http://page.math.tu-berlin.de/~kant/teaching/hess/krypto-ws2006/des.htm
#ik = 1383827165325090801
#pt = [81985529216486895]

#ik = 4702111234474983745
#pt = [6071216524903076929]

puts "Input bits: #{(pt[0].to_s(2))}"
puts "Key bits: #{(ik.to_s(2))}"

ct = des(ik, pt, 'e')

puts "C: #{(ct[0].to_s(2))}"
puts "C: 0x#{(ct[0].to_s(16))}"
puts "C: #{(ct[0].to_s)}"
s = [ct[0].to_s].pack('m') # print in base 64
puts s

pt[0] = ct[0]

#ct = des(ik, pt, 'd')

puts "C: #{(ct[0].to_s(2))}"
puts "C: 0x#{(ct[0].to_s(16))}"
puts "C: #{(ct[0].to_s)}"
s = [ct[0].to_s].pack('m') # print in base 64
puts s
