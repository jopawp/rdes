# rdes.rb
## Author: Matthew A. Weidner

Final project, MAT-260 Cryptology.
A from scratch reference implementation of the obsolete Data Encryption Standard cryptographic algorithm with key generator.